import React, {Component} from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  AppState,
  TouchableWithoutFeedback
} from 'react-native';
import { baseUrl } from '../components/Global';
import { fontSizeMyPostCenterText } from '../components/Global';
import { vLineMyPostStyle } from '../components/Global';
import Background from '../components/Background';
import Header from '../components/Header';
import {Card} from 'react-native-elements';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {Appbar,Searchbar,Button,Badge} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';
import DropDownPicker from 'react-native-dropdown-picker';
import {theme} from '../core/theme';
import TextInput from '../components/TextInput';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import {Calendar} from 'react-native-calendars';
import moment from 'moment';

//svgs
import Home from '../assets/Home';
import Employee from '../assets/Employee';
import EmployeeGray from '../assets/EmployeeGray';
import CustomerIcon from '../assets/CustomerIcon';
import FilterSettings from '../assets/FilterSettings';
import Download from '../assets/Download';
import Checked from '../assets/Checked';
import Search from '../assets/Search';
import Unchecked from '../assets/Unchecked';

class MyContractFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      loading: 'true',
      spinner: false,
      jsonData: {},
      token: '',
      productItem:[],
      vCalendar:true
    };
    
  }
 
  makeRequest = () => {
     this.setState({
      spinner: !this.state.spinner,
    });

    var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/dashboard';
    var bearer = 'Bearer ' + this.state.token;
    fetch(url, {
      method: 'POST',
      headers: {
        authentication: bearer,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({jsonData: responseJson.data, loading: false});
        this.setState({
          spinner: false,
        });
      })
      .catch((error) =>
      
        this.setState({
          isLoading: false,
          message: 'Something bad happened ' + error,
        }),
      );
  };


  onClickCancel=()=>{

    this.props.navigation.goBack()
    
  }
  onClickApply=()=>{

  }

  onClickCustomCalender =()=>{


  }



  render() {
    const jsonDashboard = this.state.jsonData;

  const currentDate = moment(new Date()).format('YYYY-MM-DD');
  const maximumDate = moment(currentDate).subtract(1, 'day').format('YYYY-MM-DD');
  const minimumDate = '2000-01-01'

  const onDayPress = day => {
    // if (isStartDate) setMillRegistrationDate(day.dateString);
    // else setReminderDate(day.dateString);

  };


    // this.setState()
   

    return (
      <Background>
        
        <View
          style={{
            flex: 1,
            width: '100%',
            height:'100%',
            position: 'relative',
            marginTop: -40,
            backgroundColor:'white'
          }}>

          <Spinner
            visible={this.state.spinner}
            color="#085cab" />

              <View style={{width: '100%',height:55, marginTop: 40}}>
                <Appbar.Header style={{backgroundColor: 'transparent'}} >
                    <Appbar.BackAction color='black' onPress={() => this.props.navigation.goBack()} />
                    <Appbar.Content
                      style={{alignItems: 'center'}}
                      color="black"
                      title="Filter"
                      titleStyle={{fontSize:20,fontWeight:'bold'}}
                    />
                    <Appbar.Action icon='notification-clear-all' color="transparent" onPress={() => {
                      this.setState({isFilterShow:true}); }} />
                  </Appbar.Header>
              </View>




 <View style={{width: '100%',height:'86%',paddingBottom:30, marginTop:10,backgroundColor: 'white',borderTopLeftRadius:20,borderTopRightRadius:20}}>
              <ScrollView>
                    <View style={{marginTop:20}}>



      <View style={{marginLeft:'5%',marginRight:'5%'}}>
                    
                    <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            
                            textAlignVertical:'center'}}>By time duration</Text>

                    <View style={{flexDirection:'row',marginTop:10}}>


                  <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center'}}>Weekly</Text>

                            <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center',
                            marginLeft:10,marginRight:10}}>Monthly</Text>

<TouchableOpacity onPress={() => this.onClickCustomCalender()}>
                            <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center'}}>Custom</Text>

                            </TouchableOpacity>
                    </View>


              </View>

                    <View style={{marginLeft:'5%',marginRight:'5%',marginTop:20}}>
                    
                    <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            
                            textAlignVertical:'center'}}>By Product</Text>

                    {
                      /*<View style={{flexDirection:'row',marginTop:10}}>


                  <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center'}}>Shankar 6</Text>

                            <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center',
                            marginLeft:10,marginRight:10}}>MECH-1</Text>

                            <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center'}}>Bunny</Text>
                    </View>


                    <View style={{flexDirection:'row',marginTop:10}}>


                  <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center'}}>J34</Text>

                            <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center',
                            marginLeft:10,marginRight:10}}>V797</Text>

                            <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            backgroundColor:'#F0F5F9',
                            borderRadius:5,
                            flex:1,
                            height:35,
                            textAlign:'center',
                            textAlignVertical:'center'}}>Y1</Text>
                    </View>


*/}

                    <View style={{height: 50,width: '100%',marginTop: 10,marginLeft: '0%',}}>
                      
                      <SelectDropdown
                          data={this.props.route.params.productList}
                          defaultValue={this.props.route.params.productList[0]}
                          onSelect={(selectedItem, i) => {
                            console.log(selectedItem)
                            // this.changeProduct(selectedItem)
                            
                          }}
                        
                        buttonStyle={styles.dropdown3BtnStyle}
                        renderCustomizedButtonChild={(selectedItem, index) => {
                          return (
                            <View style={styles.dropdown3BtnChildStyle}>
                              <Text style={styles.dropdown3BtnTxt}>
                                {selectedItem ? selectedItem.label : this.state.dropdownPlaceholder}
                              </Text>
                            </View>
                          );
                        }}
                        renderDropdownIcon={() => {
                          return (
                            <FontAwesome name="chevron-down" color={"black"} size={14} style={{marginRight:20}} />
                          );
                        }}
                        dropdownIconPosition={"right"}
                       
                        dropdownStyle={styles.dropdown3DropdownStyle}
                        rowStyle={styles.dropdown3RowStyle}
                        renderCustomizedRowChild={(item, index) => {
                          return (
                            <View style={styles.dropdown3RowChildStyle}>
                              
                              <Text style={styles.dropdown3RowTxt}>{item.label}</Text>
                            </View>
                          );
                        }}
                        /></View>


              </View>


<View style={{marginLeft:'5%',marginRight:'5%',marginTop:20}}>
                    
                    <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:12,
                            
                            textAlignVertical:'center'}}>By Seller</Text>

                            </View>



              <View style={{flexDirection: 'row', backgroundColor:'#F0F5F9',marginLeft:'5%',marginRight:'5%',height:50,marginTop:10,borderRadius:5,alignItems:'center',}}>


              <Search  style={{ width: 30,
                          height: 30,
                          marginLeft:10,
                          marginRight:0}} />


                          <TextInput
                          theme={{
                               colors: {
                                          placeholder: 'transparent', text: 'black', primary: 'transparent',
                                          underlineColor: 'transparent', background: 'transparent'
                                  }
                            }}
                            label="Search Seller"
                            style={{width:'100%',height:45,backgroundColor:'transparent'}}
                            value={"Search Seller"}
                            underlineColor={"transparent"}
                            underlineColorAndroid="transparent"
                            
                          />

              </View>


              <View style={{flexDirection: 'row',marginLeft:'5%',marginTop:10,marginRight:'5%',height:40,alignItems:'center',}}>
                   
                  <Checked  style={{ width: 30,
                          height: 30,
                          marginRight:10}} />


  

                  <View style={{flex:1}}>
                     <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:fontSizeMyPostCenterText,
                            textAlign:'left',
                            textAlignVertical:'center'}}>Ada Perry</Text>

                    <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:fontSizeMyPostCenterText,
                          
                            textAlign:'left',
                            textAlignVertical:'center'
                            ,opacity:.5}}>New York City</Text>

                  </View>

                  </View>


             <View style={{flexDirection: 'row',marginLeft:'5%',marginTop:10,marginRight:'5%',height:40,alignItems:'center',}}>


                   
                  <Unchecked style={{ width: 30,
                          height: 30,
                          
                          marginRight:10}} />


  

                  <View style={{flex:1}}>
                     <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:fontSizeMyPostCenterText,
                            textAlign:'left',
                            textAlignVertical:'center'}}>Alex McCaddy</Text>

                    <Text numberOfLines={1} 
                          ellipsizeMode='tail' 
                          style={{flex: 1,
                            color:theme.colors.textColor,
                            fontSize:fontSizeMyPostCenterText,
                          
                            textAlign:'left',
                            textAlignVertical:'center'
                            ,opacity:.5}}>Los Angeles</Text>

                  </View>



                    
                  
              </View>



                    </View>
              </ScrollView>


                        <View style={{flexDirection:'row',marginLeft:'5%'}}>

                        <View style={{flex:1}}>

                         <Button mode="contained" 
                                        uppercase={false} 
                                        contentStyle={{ height: 50 }} 
                                        style={{ width:'90%',borderColor:theme.colors.primary,borderWidth:2,backgroundColor:'white'}}  
                                        labelStyle={{fontSize:18,color:theme.colors.primary,}}  
                                        onPress={() => this.onClickCancel()}>
                                    Cancel
                                  </Button>

                        </View>

                        <View style={{flex:1}}>
                         <Button mode="contained" 
                                        uppercase={false} 
                                        contentStyle={{ height: 50 }} 
                                        style={{ width:'90%', }}  
                                        labelStyle={{fontSize:18,color:'white'}}  
                                        onPress={() => this.onClickApply()}>
                                    Apply
                                  </Button>
                        </View>


                        </View>



              </View>



  {this.state.vCalendar && (
          <View
            style={{
              position: 'absolute',
              backgroundColor: 'transparent',
              flex: 1,
              width: '100%',
              height: '100%',
              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Calendar
              current={currentDate}
              minDate={minimumDate}
              maxDate={maximumDate}
              style={styles.calendar}
              onDayPress={onDayPress}
              pastScrollRange={24}
            />
          </View>
        )}

              </View>

      </Background>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    top:0
  },
  container2:{
    marginTop:'2%',
    width:'90%',
    height:'86%',
    marginLeft:'5%',
    marginRight:'5%',
    backgroundColor: 'white',
    borderColor:'white',
    borderWidth: 1,
    borderRadius:20,
    alignItems:'flex-start',
  },
   btnActiveContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    borderBottomWidth :2,
    borderBottomColor: theme.colors.primary
  },
  btnCompletedContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    borderBottomWidth :1,
    borderBottomColor:'gray',
    opacity:.5,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    borderBottomWidth :2,
    borderBottomColor: '#57a3f5',
    marginLeft:1,
  },
  buttonContainer2: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    marginLeft:1,
    marginRight:1,
    opacity:.4
  },
  spinnerTextStyle: {
    color: '#000',
  },
  module_parent_view: {
    width: '100%',
  },
  module_label_header: {
    fontWeight: 'bold',
    fontSize: 20,
    justifyContent: 'center',
    color: '#2DA3FC',
  },
  module_card2: {
    height: 70,
    width:'90%',
    position:'absolute',
    backgroundColor: 'white',
    borderRadius: 35,
    borderColor: '#57a3f5',
    borderWidth:1,
    elevation: 5,
    alignItems:'center',
    alignSelf:'center',
    top:80
  },
  allbid:{
    flexDirection: 'row',
    marginLeft:'5%',
    marginTop:'5%'
  },
  bidedItem:{
      height: 120,
      width:'90%',
      backgroundColor: 'white',
      borderRadius: 0,
      borderColor: '#57a3f5',
      borderWidth:1,
      elevation: 5,
      marginLeft:'5%',
      marginTop:15,
      flexDirection: 'row',
  },
  bidedProduct:{
    width:'60%',
    height:'85%',
    marginLeft:'2%',
    marginTop:'3%',
    alignItems:'flex-start',
    
  },
  bidedQuantity:{
    width:'35%',
    height:'85%',
    marginTop:'3%',
    textAlign: 'center',
    alignItems: 'center',
    textAlignVertical: 'center'
  },

  titleText:{
    flex: 1,
    color:'#2DA3FC',
    fontWeight:'bold'
  },
  allbidValue:{
    flexDirection: 'row',
    marginLeft:'5%',
    marginTop:'1%'
  },
   titleTextValue:{
    flex: 1,
    color:'#2DA3FC',
    fontSize:12
  },
  scrollViewStyle: {
    width: '100%',
    flex: 1,
    backgroundColor:'white'
  },
    dealTopMainContainer: {
    flexDirection: 'row',
    top:0,
    marginLeft:'5%',
    marginRight:'5%'
  },

  dealBtnEnable: {
    flex: 1,
    width:'100%',
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor:'#69BA53',
    marginLeft:0,
    marginRight:5,
    marginTop:10,
    borderRadius:5,
  },
    dealBtnDisable: {
    flex: 1,
    width:'100%',
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor:'#F0F5F9',
    marginLeft:5,
    marginRight:0,
    marginTop:10,
    borderRadius:5,
  },
  dealTopBoxTextView:{
    height:40,
    width:'100%',
    textAlign: 'center',
    alignItems: 'center',
    textAlignVertical: 'center',
    color:'white'
  },
  dealTopBoxTextViewDisable:{
    height:40,
    width:'100%',
    textAlign: 'center',
    alignItems: 'center',
    textAlignVertical: 'center',
    color:'#343434'
  },
  
  dropdown3BtnStyle: {
    width: "100%",
    height: 50,
    backgroundColor: "#FFF",
    paddingHorizontal: 0,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#444",
    left:0  
  },
  dropdown3BtnChildStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 0,
  },
  dropdown3BtnImage: { width: 45, height: 45, resizeMode: "cover" },
  dropdown3BtnTxt: {
    color: "black",
    textAlign: "center",
    fontWeight: "normal",
    fontSize: 16,
    marginHorizontal: 0,

  },
  dropdown3DropdownStyle: { backgroundColor: "white" },
  dropdown3RowStyle: {
    backgroundColor: "#fff",
    borderBottomColor: "#444",
    height: 50
  },
  dropdown3RowChildStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: 0,
  },
  dropdownRowImage: { width: 45, height: 45, resizeMode: "cover" },
  dropdown3RowTxt: {
    color: "#000",
    textAlign: "center",
    fontWeight: "normal",
    fontSize: 16,
    marginHorizontal: 0,
    width:'100%'
  },

});

export default MyContractFilter;
